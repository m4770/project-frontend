# Frontend

Este es el repositorio del frontend del proyecto del equipo 7 grupo 32 de mision tic 2022

## Dependencias de desarrollo

### ESLint

ESLint es una herramienta para analizar nuestro código para revisar si presentamos errores de sintaxis
Es necesario instalar la extensión de VSCode
[Extensión ESlint]('https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint')

### Prettier

Es una herramienta que permite mantener una uniformidad a la hora de programar, es necesario instalar
la extensión
[Extensión Prettier]('https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode')

## Manual de coducta del código

### Componentes

Los componentes debe crearse:

- En la carpeta components
- Deben crearse en su propia carpeta
- El nombre de la carpeta debe ser igual al archivo js
- Su nombre debe ser en inglés
