import { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Container, Form, Nav, Alert } from 'react-bootstrap';

import AuthContext from 'src/context/AuthContext';
const defaultValues = {
	email: "",
	password: ""
}

const Login = () => {
	const { handleLogin } = useContext(AuthContext);

	const [user, setUser] = useState(defaultValues);
	const [show, setShow] = useState(false)

	const handleChange = (e) => {
		setUser({ ...user, [e.currentTarget.name]: e.currentTarget.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		const res = await handleLogin(user);
		if (res.status === 200) {
			setShow(false);
			const json = await res.json();
			localStorage.setItem('token', json.token)
			setUser(defaultValues);
		} else {
			setShow(true);
		}
	};

	return (
		<Container className="App-header mw-100">
			<Alert variant="danger" show={show}>
				Correo o contraseñas incorrectas
			</Alert>
			<Card className="text-center">
				<Card.Header>Iniciar sesión</Card.Header>
				<Card.Body>
					<Form onSubmit={handleSubmit}>
						<Form.Group className="mb-3" controlId="email">
							<Form.Label>Correo electrónico</Form.Label>
							<Form.Control
								value={user.email}
								onChange={handleChange}
								name="email"
								type="email"
								placeholder="Ingrese su correo"
								required
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="password">
							<Form.Label>Contraseña</Form.Label>
							<Form.Control
								value={user.password}
								onChange={handleChange}
								name="password"
								type="password"
								placeholder="Ingrese la contraseña"
								required
							/>
						</Form.Group>

						<Button variant="primary" type="submit">
							Login
						</Button>
					</Form>

				</Card.Body>
				<Card.Footer>
					<Nav.Link as={Link} to="/crear-cuenta">
						Crear Cuenta
					</Nav.Link>
				</Card.Footer>
			</Card>
		</Container>
	);
};

export default Login;
