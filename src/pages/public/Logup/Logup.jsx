import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Container, Form, Button, Card, Nav, Col, Row } from 'react-bootstrap';

import AuthContext from 'src/context/AuthContext';

const defaultValues = { name: "", email: "", password: "", address: "", phone: "" }

const Logup = () => {
	const { handleRegister } = useContext(AuthContext)
	const [account, setAccount] = useState(defaultValues);

	const handleChange = (e) => {
		setAccount({ ...account, [e.currentTarget.name]: e.currentTarget.value });
	};

	const handleCreate = (e) => {
		e.preventDefault();
		handleRegister(account)
		setAccount(defaultValues);
	};

	return (
		<Container>
			<Card className="text-center">
				<Card.Header>Crear cuenta</Card.Header>
				<Card.Body>
					<Form onSubmit={handleCreate}>
						<Row>
							<Col>
								<Form.Group className="mb-3" controlId="name">
									<Form.Label>Nombre</Form.Label>
									<Form.Control
										value={account.name}
										onChange={handleChange}
										name="name"
										type="text"
										placeholder="Ingrese su nombre completo"
										required
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group className="mb-3" controlId="address">
									<Form.Label>Dirección</Form.Label>
									<Form.Control
										value={account.address}
										onChange={handleChange}
										name="address"
										type="text"
										placeholder="Ingrese su dirección"
										required
									/>
								</Form.Group>
							</Col>
						</Row>
						<Row>
							<Col>
								<Form.Group className="mb-3" controlId="email">
									<Form.Label>Correo electrónico</Form.Label>
									<Form.Control
										value={account.email}
										onChange={handleChange}
										name="email"
										type="email"
										placeholder="Ingrese su correo"
										required
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group className="mb-3" controlId="password">
									<Form.Label>Contraseña</Form.Label>
									<Form.Control
										value={account.password}
										onChange={handleChange}
										name="password"
										type="password"
										placeholder="Ingrese su contraseña"
										required
									/>
								</Form.Group>
							</Col>
						</Row>
						<Row>
							<Col>
								<Form.Group className="mb-3" controlId="phone">
									<Form.Label>Télefono</Form.Label>
									<Form.Control
										value={account.phone}
										onChange={handleChange}
										name="phone"
										type="text"
										placeholder="Ingrese su teléfono"
										required
									/>
								</Form.Group>
							</Col>
							<Col></Col>
						</Row>

						<Button variant="primary" type="submit">
							Crear cuenta
						</Button>
					</Form>
				</Card.Body>
				<Card.Footer>
					<Nav.Link as={Link} to="/">
						Iniciar sesión
					</Nav.Link>
				</Card.Footer>
			</Card>
		</Container>
	);
};

export default Logup;
