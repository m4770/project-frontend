import { useState } from 'react';
import { apiCreatePost } from 'src/context/api';
import { Container, Form, Button, Card, Col, Row, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router';

const defaultValues = { title: "", category: "", quantity: 0, unity: "", url_img: "" }

const CreatePost = () => {
    const [post, setPost] = useState(defaultValues);
    const [show, setShow] = useState(false)
    const [error, setError] = useState(false)
    const navigate = useNavigate();
    const handleChange = (e) => {
        setPost({ ...post, [e.currentTarget.name]: e.currentTarget.value });
    };

    const handleCreate = (e) => {
        e.preventDefault();
        const token = localStorage.getItem('token');
        fetch(apiCreatePost, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(post)
        })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                setShow(true);
                setTimeout(()=>{
                    navigate("/mis-publicaciones");
                } , 3000);
            }).catch(error => {
                console.log(error);
                setError(true);
            })
    };

    return (
        <Container>
            <Alert variant={error ? "danger" : "success"} show={show}>
                {!error ? "Publicación creado con éxito" : "Error al crear la publicación"}
            </Alert>
            <Card className="text-center">
                <Card.Header>Crear una publicación</Card.Header>
                <Card.Body>
                    <Form onSubmit={handleCreate}>
                        <Row>
                            <Col>
                                <Form.Group className="mb-3" controlId="title">
                                    <Form.Label>Título publicación</Form.Label>
                                    <Form.Control
                                        value={post.title}
                                        onChange={handleChange}
                                        name="title"
                                        type="text"
                                        placeholder="Ingrese un title"
                                        required
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="category">
                                    <Form.Label>Categoría</Form.Label>
                                    <Form.Control
                                        value={post.category}
                                        onChange={handleChange}
                                        name="category"
                                        type="text"
                                        placeholder="Ingrese la categoría"
                                        required
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group className="mb-3" controlId="quantity">
                                    <Form.Label>Cantidad</Form.Label>
                                    <Form.Control
                                        value={post.quantity}
                                        onChange={handleChange}
                                        name="quantity"
                                        type="number"
                                        placeholder="Ingrese la cantidad"
                                        required
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="unity">
                                    <Form.Label>Unidad de medida</Form.Label>
                                    <Form.Control
                                        value={post.unity}
                                        onChange={handleChange}
                                        name="unity"
                                        type="text"
                                        placeholder="Ingrese la unidad de medida"
                                        required
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group className="mb-3" controlId="url_img">
                                    <Form.Label>Imagen</Form.Label>
                                    <Form.Control
                                        value={post.url_img}
                                        onChange={handleChange}
                                        name="url_img"
                                        type="text"
                                        placeholder="Ingrese la url de la imagen"
                                        required
                                    />
                                </Form.Group>
                            </Col>
                        </Row>

                        <Button variant="primary" type="submit">
                            Crear Publicación
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </Container>
    );
}

export default CreatePost
