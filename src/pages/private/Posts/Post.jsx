import React from 'react';
import useGetPostsByUser from 'src/hooks/useGetPostsByUser';
import PostCard from 'src/components/PostCard/PostCard';

const Post = () => {
    const posts = useGetPostsByUser();
    return (
        <main className="grid-post">
            {posts.map((post) => {
                return (
                    <PostCard key={post._id} post={post}  active={true}/>
                );
            })}
        </main>
    );
};

export default Post;
