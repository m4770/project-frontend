import React from 'react';
import PostCard from 'src/components/PostCard/PostCard';
import useGetPost from 'src/hooks/useGetPost';

const Home = () => {
	const posts = useGetPost();
	return (
		<main className="grid-post">
			{
				posts.map(post => {
					return <PostCard key={post._id} post={post} />
				})
			}
		</main>
	);
};

export default Home;
