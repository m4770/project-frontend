// Importaciones
import { useContext } from "react";

// Context
import AuthContext from "src/context/AuthContext";

import AuthRouter from "./routers/AuthRouter";
import UnAuthRouter from "./routers/UnAuthRouter";

// Estilos
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

function App() {
	const { auth } = useContext(AuthContext)
	return (
		auth ? <AuthRouter /> : <UnAuthRouter />
	);
}

export default App;
