import { Route, Routes } from 'react-router';
import { Container } from 'react-bootstrap';
// Páginas
import Posts from 'src/pages/private/Posts/Post';
import Home from 'src/pages/private/Home/Home';

import Header from 'src/components/Header/Header';
import CreatePost from 'src/pages/private/CreatePost/CreatePost';

const AuthRouter = () => {
	return (
		<>
			<Header />
			<Container className="App mw-100">
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/mis-publicaciones" element={<Posts />} />
					<Route path="/crear-publicacion" element={<CreatePost />} />
				</Routes>
			</Container>
		</>
	);
};

export default AuthRouter;
