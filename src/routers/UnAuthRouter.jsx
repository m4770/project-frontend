import { Route, Routes } from 'react-router';
import { Container } from 'react-bootstrap';

import Login from 'src/pages/public/Login/Login';
import Logup from 'src/pages/public/Logup/Logup';

const UnAuthRouter = () => {
	return (
		<Container className="App mw-100">
			<Routes>
				<Route path="/" element={<Login />} />
				<Route path="crear-cuenta" element={<Logup />} />
			</Routes>
		</Container>
	);
};

export default UnAuthRouter;
