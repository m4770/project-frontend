import { apiGetAllPost, apiGetPostsByUser } from 'src/context/api';

const fromApiResponseToPost = (apiResponse) => {
    const { posts = [] } = apiResponse;
    if (Array.isArray(posts)) {
        return posts;
    } else {
        return [];
    }
};

export function getPostsByUser() {
    const token = localStorage.getItem('token');

    return fetch(apiGetPostsByUser, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
        .then((res) => res.json())
        .then(fromApiResponseToPost);
}


export default function getPosts() {
    return fetch(apiGetAllPost)
        .then((res) => res.json())
        .then(fromApiResponseToPost);
}