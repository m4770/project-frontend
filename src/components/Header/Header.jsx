import React, { useContext } from 'react';

import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AuthContext from 'src/context/AuthContext';

import './Header.css';

const Header = () => {
	const { handleLogout } = useContext(AuthContext)
	return (
		<Navbar className="header" variant="light" sticky="top">
			<Container className="header__elements">
				<Navbar.Brand as={Link} to="/">
					3R
				</Navbar.Brand>
				<Nav className="me-auto">
					<Nav.Link as={Link} to="/">
						Publicaciones
					</Nav.Link>
					<Nav.Link as={Link} to="/mis-publicaciones">
						Mis publicaciones
					</Nav.Link>
				</Nav>
				<Navbar className="justify-content-end">
					<Nav.Link as={Link} to="/crear-publicacion">
						Crear una publicación
					</Nav.Link>
					<Navbar.Text onClick={handleLogout}>
						Cerrar sesión
					</Navbar.Text>
				</Navbar>
			</Container>
		</Navbar>
	);
};

export default Header;
