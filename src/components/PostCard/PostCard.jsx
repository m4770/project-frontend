import React from 'react';
import { Card, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { useNavigate } from 'react-router';
import { apiDeletePost } from 'src/context/api';

import './PostCard.css';

const PostCard = ({ post, active }) => {
    const { title, category, quantity, unity, url_img, _id } = post;
    const navigate = useNavigate();
    const handleDeletePost = () => {
        const token = localStorage.getItem('token');
        fetch(apiDeletePost, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({ id: _id })
        })
            .then(res => res.json())
            .then(res => {
                console.log(res);
                navigate("/");
            }).catch(error => {
                console.log(error);
            })
    }
    return (
        <Card style={{ width: '18rem' }} className="card-post">
            <div className="card-post-img" >
                <Card.Img variant="top" src={url_img} />
            </div>
            <Card.Body>
                <Card.Title className="black">{title}</Card.Title>
            </Card.Body>
            <ListGroup className="list-group-flush">
                <ListGroupItem>{category}</ListGroupItem>
                <ListGroupItem>{quantity + " " + unity}</ListGroupItem>
            </ListGroup>
            {
                active ? <Card.Body className="d-flex justify-content-between">
                    <Button variant="danger" onClick={handleDeletePost}>Eliminar</Button>
                    <Button variant="primary">Editar</Button>
                </Card.Body> : null
            }

        </Card>
    )
}

export default PostCard
