const server = "https://backend-3r-2.herokuapp.com/";

const apiRegister = `${server}/user`
const apiLogin = `${server}/user/auth`

const apiGetAllPost = `${server}/posts`
const apiGetPostsByUser = `${server}/post`
const apiCreatePost = `${server}/post` 
const apiDeletePost = `${server}/post` 

export  {
    apiRegister,
    apiLogin,
    apiGetAllPost,
    apiGetPostsByUser,
    apiCreatePost,
    apiDeletePost
};