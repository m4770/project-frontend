import { createContext, useState, useEffect } from 'react';
import { useNavigate } from 'react-router';
import { apiLogin, apiRegister } from 'src/context/api';

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
    const [auth, setAuth] = useState(false);
    const navigate = useNavigate(); 

    useEffect(() => {
        const token = localStorage.getItem("token");
        if (token) {
            setAuth(true);
        }
    }, []);


    const handleRegister = (newUser) => {
        // Realizar petición al servidor
        fetch(apiRegister, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newUser),

        }).then(async res => {
            if (res.status === 201) {
                const json = await res.json();
                const token = json.token;
                localStorage.setItem("token", token)
                setAuth(true);
                navigate('/');
            } else {
                console.log("no se registro correctamente")
            }
        }).catch(error => {
            console.log(error)
        });
    };

    const handleLogin = async (user) => {
        const res = await fetch(apiLogin, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(user),
        })
        if (res.status === 200) {
            setAuth(true);
            navigate('/');
        }
        return res;
    };

    const handleLogout = ()=>{
        setAuth(false);
        localStorage.setItem("token", "");
        navigate('/');
    }

    const data = { auth, handleLogin, handleRegister, handleLogout };

    return <AuthContext.Provider value={data}>{children}</AuthContext.Provider>;
};

export { AuthProvider };
export default AuthContext;
