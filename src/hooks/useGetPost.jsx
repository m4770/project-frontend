import { useState, useEffect } from 'react';
import getPosts from 'src/services/getPosts'

const useGetPosts = () => {
    const [posts, setPosts] = useState([])

    useEffect(() => {
        getPosts().then((res) => {
            setPosts(res);
        });
    }, []);

    return posts;
}

export default useGetPosts;