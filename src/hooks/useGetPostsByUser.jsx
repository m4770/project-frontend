import { useState, useEffect } from 'react';
import { getPostsByUser } from 'src/services/getPosts'

const useGetPostsByUser = () => {
    const [posts, setPosts] = useState([])

    useEffect(() => {
        getPostsByUser().then((res) => {
            setPosts(res);
        });
    }, []);

    return posts;
}

export default useGetPostsByUser;